import pytest

# parametrization
@pytest.mark.parametrize("num",[99,101,102])

def test_101(num):
    print("testing 101...")
    assert 100+1 == num
