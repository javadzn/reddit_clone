
from typing import List
from . database import get_db, database_engine, SessionTemplate
from fastapi import Depends, FastAPI, HTTPException, Response, status
from sqlalchemy.orm import Session
from . import models, schemas
from sqlalchemy.exc import IntegrityError

# Create the tables if they don't exists yet
models.Base.metadata.create_all(bind=database_engine)

app = FastAPI()  # Run the Server

# Instantiate a session to execute query from py instead of Postman
my_session = SessionTemplate()


@app.get('/posts', response_model=List[schemas.BlogPost_Response])
def get_posts(db: Session = Depends(get_db)):
    all_posts = db.query(models.BlogPost).all()
    return all_posts


#Select one blogpost from its id
@app.get('/posts{uri_id}', response_model=schemas.BlogPost_Response)
def get_post(uri_id: int, db: Session =Depends(get_db)):
    corresponding_post= db.query(models.BlogPost) .filter(models.BlogPost.id== uri_id) .first()
    if not corresponding_post:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Not corresponding post was found with id:{uri_id}")
    return corresponding_post

# Create a new BlogPost row
@app.post('/posts', response_model=schemas.BlogPost_Response)
def create_post(post_body: schemas.BlogPostPy, db:Session=Depends(get_db)):
    new_post= models.BlogPost(**post_body.dict())
    db.add(new_post)
    db.commit()
    db.refresh(new_post)
    return new_post


####### POST/CREATE operation #######
@app.post("/posts", response_model=schemas.BlogPost_Response,status_code=status.HTTP_201_CREATED) # decorator + status as a parameter
def create_posts(post_body:schemas.BlogPostPy, db:Session=Depends(get_db)):
   try:
         new_post=models.BlogPost(**post_body.dict())
         db.add(new_post)
         db.commit()
         db.refresh(new_post)
         status_code=status.HTTP_201_CREATED
         return  new_post 
   except IntegrityError as err:
         raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                           detail=f"Foregn key violation with writer_id:{post_body.writer_id}")


@app.delete("/posts/{uri_id}", status_code=status.HTTP_204_NO_CONTENT)
def delete_post(uri_id: int, db: Session = Depends(get_db)):
   post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
   if not post:
       raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"Post not found with id: {uri_id}")
   db.delete(post)
   db.commit()
   return Response(status_code=status.HTTP_204_NO_CONTENT)




# @app.delete('/posts/{uri_id}', response_model=schemas.BlogPost_Response)
# def delete_post(uri_id: int, db: Session = Depends(get_db)):
#     corresponding_post = db.query(models.BlogPost).filter(models.BlogPost.id == uri_id).first()
#     if not corresponding_post:
#         raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f"No corresponding post was found with id: {uri_id}")
#     db.delete(corresponding_post)
#     db.commit()
#     return corresponding_post



# uvicorn v3_orm.main:app --reload