
from datetime import datetime
from pydantic import BaseModel, EmailStr

# UserPy
# Pydantic schema for POST Body validation
class UserPy(BaseModel):
    email: EmailStr # str is too general (random text)
    password: str

# Pydantic schema Reponse
class User_Response(UserPy):
    id: int
    created_at: datetime
    class Config: # Important for pydantic model/schema translation
        orm_mode = True
    
# BlogPostPy
# Pydantic schema for POST Body validation
class BlogPostPy(BaseModel): 
    title: str
    content: str
    #published: bool = True
    writer_id: int
    
# Pydantic schema Reponse
class BlogPost_Response(BlogPostPy):  
    id: int
    created_at: datetime
    # writer_id: int
    writer: User_Response
    class Config: # Important for pydantic model/schema translation
        orm_mode = True

